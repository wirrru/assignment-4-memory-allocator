#include "mem.h"
#include <assert.h>
#include <stdio.h>

void test_successful_allocation() {
    void* ptr = _malloc(100);
    assert(ptr != NULL);
}

void test_free_one_block() {
    void* ptr1 = _malloc(50);
    _malloc(30);
    _free(ptr1);
    void* new_ptr = _malloc(20);
    assert(new_ptr != NULL);
}

void test_free_two_blocks() {
    void* ptr1 = _malloc(40);
    _malloc(30);
    void* ptr3 = _malloc(20);
    _free(ptr1);
    _free(ptr3);
    void* new_ptr1 = _malloc(15);
    void* new_ptr2 = _malloc(25);
    assert(new_ptr1 != NULL && new_ptr2 != NULL);
}

void test_memory_exhausted_extend_region() {
    _malloc(50);
    _malloc(30);
    void* new_ptr = _malloc(1000);
    assert(new_ptr != NULL);
}

void test_memory_exhausted_new_region() {
    void* ptr1 = _malloc(50);
    void* ptr2 = _malloc(30);
    _malloc(1000);
    _free(ptr1);
    _free(ptr2);
    void* new_ptr2 = _malloc(1000);
    assert(new_ptr2 != NULL);
}

int main() {
    test_successful_allocation();
    printf("Test 1 passed\n");

    test_free_one_block();
    printf("Test 2 passed\n");

    test_free_two_blocks();
    printf("Test 3 passed\n");

    test_memory_exhausted_extend_region();
    printf("Test 4 passed\n");

    test_memory_exhausted_new_region();
    printf("Test 5 passed\n");

    return 0;
}
